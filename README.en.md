# jt808-server

#### Description
这是笔者所在公司根据交通部808协议编写的网关服务端程序，实现了808协议当中的80%多的主要功能，项目采用spring boot、netty、redis等等技术，并且该程序支持集群，可以与ngixn结合进行负载均衡，支持在单机10000万左右的tcp连接，经验证在生产环境中可以同时支持3000左右的并发连接完全没有问题。有什么问题的可以联系我，联系QQ203399999，这是我在github发布的第一个能用于实际项目的程序，关注的朋友们麻烦给个星星

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)