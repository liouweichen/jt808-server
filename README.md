# jt808-server

#### 介绍
这是笔者所在公司根据交通部808协议编写的网关服务端程序，实现了808协议当中的80%多的主要功能，项目采用spring boot、netty、redis等等技术，并且该程序支持集群，可以与ngixn结合进行负载均衡，支持在单机10000万左右的tcp连接，经验证在生产环境中可以同时支持3000左右的并发连接完全没有问题。有什么问题的可以联系我，联系QQ203399999，这是我在github发布的第一个能用于实际项目的程序，关注的朋友们麻烦给个星星

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)