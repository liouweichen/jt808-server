package org.zhtkj.framework.codec;

import io.netty.buffer.ByteBuf;

import static org.zhtkj.framework.enums.DataType.*;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.commons.bean.BeanUtils;
import org.zhtkj.framework.commons.transform.BCD8421Operator;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.Header;
import org.zhtkj.framework.message.PackageData;

/**
 * 基础消息解码
 */
public abstract class MessageDecoder extends AbstractMessageCoder {

    public MessageDecoder(Charset charset) {
        super(charset);
    }

    public abstract Header decodeHeader(ByteBuf buf);

    public <T extends PackageData<?>> T decodeBody(ByteBuf buf, Class<T> targetClass) {
        return decode(buf, targetClass);
    }

    public <T> T decode(ByteBuf buf, Class<T> targetClass) {
        T result = BeanUtils.newInstance(targetClass);
        PropertyDescriptor[] pds = getPropertyDescriptor(targetClass);
        for (PropertyDescriptor pd : pds) {
            Method readMethod = pd.getReadMethod();
            Property prop = readMethod.getDeclaredAnnotation(Property.class);
            int length = getLength(result, prop);
            if (!buf.isReadable(length)) {
                break;
            }
            if (length == -1)
                length = buf.readableBytes();
            Object value = null;
            try {
                value = read(buf, prop.type(), length, pd);
            } catch (Exception e) {
                e.printStackTrace();
            }
            BeanUtils.setValue(result, pd.getWriteMethod(), value);
        }
        return result;
    }

	public Object read(ByteBuf buf, DataType type, int length, PropertyDescriptor pd) {
        if (type == BYTE) {
            return (int) buf.readUnsignedByte();
        } else if (type == WORD) {
            return buf.readUnsignedShort();
        } else if (type == DWORD) {
            if (pd.getPropertyType().isAssignableFrom(Long.class))
                return buf.readUnsignedInt();
            return (int) buf.readUnsignedInt();
        } else if (type == STRING) {
            return buf.readCharSequence(length, charset).toString().trim();
        } else if (type == OBJ) {
            return decode(buf.readSlice(length), pd.getPropertyType());
        } else if (type == LIST) {
            List<Object> list = new ArrayList<Object>();
            Type clazz = ((ParameterizedType) pd.getReadMethod().getGenericReturnType()).getActualTypeArguments()[0];
            ByteBuf slice = buf.readSlice(length);
            while (slice.isReadable())
                list.add(decode(slice, (Class<?>) clazz));
            return list;
        }

        byte[] bytes = new byte[length];
        buf.readBytes(bytes);
        if (type == BCD8421) {
            return BCD8421Operator.bcd2String(bytes).trim();
        }
        return bytes;
    }
}