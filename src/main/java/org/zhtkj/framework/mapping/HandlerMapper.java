package org.zhtkj.framework.mapping;

public interface HandlerMapper {

    Handler getHandler(Integer key);

}