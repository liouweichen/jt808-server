package org.zhtkj.framework.mapping;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zhtkj.framework.annotation.Endpoint;
import org.zhtkj.framework.annotation.Mapping;
import org.zhtkj.framework.commons.ClassUtils;
import org.zhtkj.framework.commons.bean.BeanUtils;

public class DefaultHandlerMapper implements HandlerMapper {

    private Map<Integer, Handler> handlerMap = new HashMap<Integer, Handler>(55);

    public DefaultHandlerMapper(String... packageNames) {
        for (String packageName : packageNames) {
            addPackage(packageName);
        }
    }

    private void addPackage(String packageName) {
        List<Class<?>> handlerClassList = ClassUtils.getClassList(packageName, Endpoint.class);

        for (Class<?> handlerClass : handlerClassList) {
            Method[] methods = handlerClass.getDeclaredMethods();
            if (methods != null) {
                for (Method method : methods) {
                    if (method.isAnnotationPresent(Mapping.class)) {
                        Mapping annotation = method.getAnnotation(Mapping.class);
                        String desc = annotation.desc();
                        int[] types = annotation.types();
                        Handler value = new Handler(BeanUtils.newInstance(handlerClass), method, desc);
                        for (int type : types) {
                            handlerMap.put(type, value);
                        }
                    }
                }
            }
        }
    }

    public Handler getHandler(Integer key) {
        return handlerMap.get(key);
    }

}