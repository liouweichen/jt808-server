package org.zhtkj.framework.session;

import org.zhtkj.web.model.HistoryInfo;

import io.netty.channel.Channel;

public class Session {

    private String id;
    private Channel channel = null;
    // 消息流水号 word(16) 按发送顺序从 0 开始循环累加
    private int currentFlowId = 0;
    //车辆最后一次历史信息
    private HistoryInfo historyInfo;
    
    public Session() {
    }

    public static String buildId(Channel channel) {
        return channel.id().asLongText();
    }

    public static Session buildSession(Channel channel) {
        Session session = new Session();
        session.setChannel(channel);
        session.setId(buildId(channel));
        return session;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

	public HistoryInfo getHistoryInfo() {
		return historyInfo;
	}

	public void setHistoryInfo(HistoryInfo historyInfo) {
		this.historyInfo = historyInfo;
	}

	public synchronized int currentFlowId() {
        if (currentFlowId >= 0xffff)
            currentFlowId = 0;
        return currentFlowId++;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that)
            return true;
        if (that == null)
            return false;
        if (getClass() != that.getClass())
            return false;
        Session other = (Session) that;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Session [id=" + id + ", channel=" + channel + ", currentFlowId=" + currentFlowId + "]";
    }
}