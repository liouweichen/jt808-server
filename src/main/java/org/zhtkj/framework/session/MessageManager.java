package org.zhtkj.framework.session;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.zhtkj.framework.message.SyncFuture;

public enum MessageManager {

    INSTANCE;

    private Map<String, SyncFuture<Object>> map = new ConcurrentHashMap<String, SyncFuture<Object>>();


    public SyncFuture<?> receive(String key) {
        SyncFuture<Object> future = new SyncFuture<Object>();
        map.put(key, future);
        return future;
    }

    public void remove(String key) {
        map.remove(key);
    }

    public void put(String key, Object value) {
        SyncFuture<Object> syncFuture = map.get(key);
        if (syncFuture == null) {
        	return;
        }
        syncFuture.setResponse(value);
    }

}
