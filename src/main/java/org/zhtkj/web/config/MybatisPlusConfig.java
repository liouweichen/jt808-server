package org.zhtkj.web.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.config.GlobalConfig.DbConfig;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;

@Configuration
@EnableCaching
@MapperScan("org.zhtkj.web.dao")
public class MybatisPlusConfig {

    @Bean(name = "dataSource")
    @ConfigurationProperties(prefix = "spring.datasource.jt808")
    public DruidDataSource dataSource(){
        return new DruidDataSource();
    }
    
    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource());
        sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:org/zhtkj/web/dao/mapping/*.xml"));
        sqlSessionFactory.setGlobalConfig(globalConfig());
        return sqlSessionFactory.getObject();
    }


    @Bean("globalConfig")
    public GlobalConfig globalConfig() {
    	DbConfig dbCfg = new DbConfig();
    	dbCfg.setIdType(IdType.AUTO);
    	dbCfg.setDbType(DbType.MYSQL);
    	GlobalConfig conf = new GlobalConfig();
    	conf.setDbConfig(dbCfg);
    	return conf;
    }
}
