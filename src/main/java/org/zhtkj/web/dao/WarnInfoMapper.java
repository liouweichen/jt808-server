package org.zhtkj.web.dao;

import org.zhtkj.web.model.WarnInfo;

public interface WarnInfoMapper {

	/**
	 * @Description: 插入报警信息
	 * @param warnInfo
	 * @return   
	 * @return int  
	 * @throws
	 * @author Nikotesla
	 * @date 2019年2月15日
	 */
	int insertWarnInfo(WarnInfo warnInfo);
}
