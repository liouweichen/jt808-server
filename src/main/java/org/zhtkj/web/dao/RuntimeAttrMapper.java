package org.zhtkj.web.dao;

import org.zhtkj.web.model.RuntimeInfoAttr;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface RuntimeAttrMapper extends BaseMapper<RuntimeInfoAttr> {

}
