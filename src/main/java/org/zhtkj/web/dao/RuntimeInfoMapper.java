package org.zhtkj.web.dao;

import org.zhtkj.web.model.RuntimeInfo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface RuntimeInfoMapper extends BaseMapper<RuntimeInfo> {

}
