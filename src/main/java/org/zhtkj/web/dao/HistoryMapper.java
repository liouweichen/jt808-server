package org.zhtkj.web.dao;

import org.zhtkj.web.model.HistoryAttr;
import org.zhtkj.web.model.HistoryInfo;
import org.zhtkj.web.model.HistoryWarn;

public interface HistoryMapper {

	/**
	 * @Description: 插入位置汇报信息
	 * @param yearMonth
	 * @param historyInfo
	 * @return   
	 * @return int  
	 * @throws
	 * @author Nikotesla
	 * @date 2019年1月9日
	 */
	int insertHistoryInfo(HistoryInfo hisInfo);
	
	/**
	 * @Description: 插入附加信息
	 * @param yearMonth
	 * @param hisAttr
	 * @return   
	 * @return int  
	 * @throws
	 * @author Nikotesla
	 * @date 2019年1月9日
	 */
	int insertHistoryAttr(HistoryAttr hisAttr);
	
	/**
	 * @Description: 插入附加信息报警
	 * @param yearMonth
	 * @param hisWarn
	 * @return   
	 * @return int  
	 * @throws
	 * @author Nikotesla
	 * @date 2019年1月9日
	 */
	int insertHistoryWarn(HistoryWarn hisWarn);
}
