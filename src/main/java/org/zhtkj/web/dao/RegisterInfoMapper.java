package org.zhtkj.web.dao;

import org.zhtkj.web.model.RegisterInfo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface RegisterInfoMapper extends BaseMapper<RegisterInfo> {

}
