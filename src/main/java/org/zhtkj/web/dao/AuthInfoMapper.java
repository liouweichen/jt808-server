package org.zhtkj.web.dao;

import org.zhtkj.web.model.AuthInfo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AuthInfoMapper extends BaseMapper<AuthInfo> {

}
