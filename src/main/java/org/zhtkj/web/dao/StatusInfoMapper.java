package org.zhtkj.web.dao;

import org.zhtkj.web.model.StatusInfo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface StatusInfoMapper extends BaseMapper<StatusInfo> {

}
