package org.zhtkj.web.model;

import java.util.Date;

/**
 * ClassName: HistoryInfo 
 * @Description: 历史位置信息表
 * @author Nikotesla
 * @date 2019年1月14日
 */
 
public class HistoryInfo {

	private Integer historyInfoId;
	private String licensePlate;
	private String terminalId;
    private Double latitude;
    private Double longitude;
    private Integer altitude;
    private Integer speed;
    private Integer direction;
    private Integer loadStatus;
    private Integer boxStatus;
    private Integer liftStatus;
    private Integer lockStatus;
    private Integer limitStatus;
    private Integer limitLift;
    private Integer maintainType;
    private Integer statusMark;
    private Integer warnMark;
    private Date reportTime;
    private Date insertTime;
    private String yearMonth;
    
	public Integer getHistoryInfoId() {
		return historyInfoId;
	}
	public void setHistoryInfoId(Integer historyInfoId) {
		this.historyInfoId = historyInfoId;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Integer getAltitude() {
		return altitude;
	}
	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}
	public Integer getSpeed() {
		return speed;
	}
	public void setSpeed(Integer speed) {
		this.speed = speed;
	}
	public Integer getDirection() {
		return direction;
	}
	public void setDirection(Integer direction) {
		this.direction = direction;
	}
	public Integer getLoadStatus() {
		return loadStatus;
	}
	public void setLoadStatus(Integer loadStatus) {
		this.loadStatus = loadStatus;
	}
	public Integer getBoxStatus() {
		return boxStatus;
	}
	public void setBoxStatus(Integer boxStatus) {
		this.boxStatus = boxStatus;
	}
	public Integer getLiftStatus() {
		return liftStatus;
	}
	public void setLiftStatus(Integer liftStatus) {
		this.liftStatus = liftStatus;
	}
	public Integer getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(Integer lockStatus) {
		this.lockStatus = lockStatus;
	}
	public Integer getLimitStatus() {
		return limitStatus;
	}
	public void setLimitStatus(Integer limitStatus) {
		this.limitStatus = limitStatus;
	}
	public Integer getLimitLift() {
		return limitLift;
	}
	public void setLimitLift(Integer limitLift) {
		this.limitLift = limitLift;
	}
	public Integer getMaintainType() {
		return maintainType;
	}
	public void setMaintainType(Integer maintainType) {
		this.maintainType = maintainType;
	}
	public Integer getStatusMark() {
		return statusMark;
	}
	public void setStatusMark(Integer statusMark) {
		this.statusMark = statusMark;
	}
	public Integer getWarnMark() {
		return warnMark;
	}
	public void setWarnMark(Integer warnMark) {
		this.warnMark = warnMark;
	}
	public Date getReportTime() {
		return reportTime;
	}
	public void setReportTime(Date reportTime) {
		this.reportTime = reportTime;
	}
	public Date getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}
	public String getYearMonth() {
		return yearMonth;
	}
	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}
    
}
