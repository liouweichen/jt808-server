package org.zhtkj.web.model;

import java.util.Date;

/**
 * ClassName: HistoryWarn
 * 
 * @Description: 位置信息报警表
 * @author Nikotesla
 * @date 2019年1月14日
 */

public class HistoryWarn {

	private Integer historyWarnId;
	private String licensePlate;
	private String terminalId;
	private Integer loadOff;
	private Integer boxOff;
	private Integer liftOff;
	private Integer powerBreak;
	private Integer rapidAcc;
	private Integer rapidSlow;
	private Integer rapidChange;
	private Integer rapidTurn;
	private Integer impact;
	private Integer overload;
	private Integer heavySpeed;
	private Integer vioUnload;
	private Integer heavyBoxOpen;
	private Integer badDrive;
	private Integer heavyEnter;
	private Date reportTime;
	private String yearMonth;

	//初始化时将报警标志全部置为0
	public HistoryWarn() {
		this.loadOff = 0;
		this.boxOff = 0;
		this.liftOff = 0;
		this.powerBreak = 0;
		this.rapidAcc = 0;
		this.rapidSlow = 0;
		this.rapidChange = 0;
		this.rapidTurn = 0;
		this.impact = 0;
		this.overload = 0;
		this.heavySpeed = 0;
		this.vioUnload = 0;
		this.heavyBoxOpen = 0;
		this.badDrive = 0;
		this.heavyEnter = 0;
	}
	
	public Integer getHistoryWarnId() {
		return historyWarnId;
	}

	public void setHistoryWarnId(Integer historyWarnId) {
		this.historyWarnId = historyWarnId;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public Integer getLoadOff() {
		return loadOff;
	}

	public void setLoadOff(Integer loadOff) {
		this.loadOff = loadOff;
	}

	public Integer getBoxOff() {
		return boxOff;
	}

	public void setBoxOff(Integer boxOff) {
		this.boxOff = boxOff;
	}

	public Integer getLiftOff() {
		return liftOff;
	}

	public void setLiftOff(Integer liftOff) {
		this.liftOff = liftOff;
	}

	public Integer getPowerBreak() {
		return powerBreak;
	}

	public void setPowerBreak(Integer powerBreak) {
		this.powerBreak = powerBreak;
	}

	public Integer getRapidAcc() {
		return rapidAcc;
	}

	public void setRapidAcc(Integer rapidAcc) {
		this.rapidAcc = rapidAcc;
	}

	public Integer getRapidSlow() {
		return rapidSlow;
	}

	public void setRapidSlow(Integer rapidSlow) {
		this.rapidSlow = rapidSlow;
	}

	public Integer getRapidChange() {
		return rapidChange;
	}

	public void setRapidChange(Integer rapidChange) {
		this.rapidChange = rapidChange;
	}

	public Integer getRapidTurn() {
		return rapidTurn;
	}

	public void setRapidTurn(Integer rapidTurn) {
		this.rapidTurn = rapidTurn;
	}

	public Integer getImpact() {
		return impact;
	}

	public void setImpact(Integer impact) {
		this.impact = impact;
	}

	public Integer getOverload() {
		return overload;
	}

	public void setOverload(Integer overload) {
		this.overload = overload;
	}

	public Integer getHeavySpeed() {
		return heavySpeed;
	}

	public void setHeavySpeed(Integer heavySpeed) {
		this.heavySpeed = heavySpeed;
	}

	public Integer getVioUnload() {
		return vioUnload;
	}

	public void setVioUnload(Integer vioUnload) {
		this.vioUnload = vioUnload;
	}

	public Integer getHeavyBoxOpen() {
		return heavyBoxOpen;
	}

	public void setHeavyBoxOpen(Integer heavyBoxOpen) {
		this.heavyBoxOpen = heavyBoxOpen;
	}

	public Integer getBadDrive() {
		return badDrive;
	}

	public void setBadDrive(Integer badDrive) {
		this.badDrive = badDrive;
	}

	public Integer getHeavyEnter() {
		return heavyEnter;
	}

	public void setHeavyEnter(Integer heavyEnter) {
		this.heavyEnter = heavyEnter;
	}

	public Date getReportTime() {
		return reportTime;
	}

	public void setReportTime(Date reportTime) {
		this.reportTime = reportTime;
	}

	public String getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}

	/**
	 * @Description: 判断是否需要插入数据库，如果某一项报警存在才插入数据库
	 * @return   
	 * @return boolean  
	 * @throws
	 * @author Nikotesla
	 * @date 2019年1月22日
	 */
	public boolean isValid() {
		if (this.loadOff == 1 || this.boxOff == 1 || this.liftOff == 1 
				|| this.powerBreak == 1 ||  this.rapidAcc == 1 || this.rapidSlow == 1 || this.rapidChange == 1
				|| this.rapidTurn == 1 || this.impact == 1 || this.overload == 1 || this.heavySpeed == 1
				|| this.vioUnload == 1 || this.heavyBoxOpen == 1 || this.badDrive == 1 || this.heavyEnter == 1) {
			return true;
		} else {
			return false;
		}
	}
}
