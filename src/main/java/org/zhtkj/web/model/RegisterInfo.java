package org.zhtkj.web.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * ClassName: RegisterInfo 
 * @Description: 终端注册信息
 * @author Nikotesla
 * @date 2019年1月8日
 */
 
@TableName(value = "zt_register_info")
public class RegisterInfo {

	@TableId(type = IdType.INPUT)
	private String licensePlate;
	private String terminalId;
    private Integer provinceId;
    private Integer cityId;
    private String manufacturerId;
    private String terminalType;
    private String terminalNumber;
    private Integer licensePlateColor;
    private String authCode;
    
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public Integer getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public String getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(String manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	public String getTerminalType() {
		return terminalType;
	}
	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}
	public String getTerminalNumber() {
		return terminalNumber;
	}
	public void setTerminalNumber(String terminalNumber) {
		this.terminalNumber = terminalNumber;
	}
	public Integer getLicensePlateColor() {
		return licensePlateColor;
	}
	public void setLicensePlateColor(Integer licensePlateColor) {
		this.licensePlateColor = licensePlateColor;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
    
}
