package org.zhtkj.web.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * ClassName: RuntimeInfoAttr 
 * @Description: 实时位置信息附加属性表
 * @author Nikotesla
 * @date 2019年1月15日
 */
@TableName(value = "zt_runtime_info_attr")
public class RuntimeInfoAttr {

	@TableId(type = IdType.INPUT)
	private String licensePlate;
	private Integer connectMark;
	private Integer turnMark;
	private Integer revSpeed;
	private Integer loadSize;
	private String vehicleVin;
	private Integer mileage;
	private Integer oilUse;
	private Integer oilRest;
	private Integer oilInst;
	private Integer canSpeed;
	private Integer rotateSpeed;
	private Integer torque;
	private Integer voltage;
	private Integer brake;
	private Integer coolantTemp;
	private Integer urgentStatus;
	private Date urgentStatusTime;
	
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public Integer getConnectMark() {
		return connectMark;
	}
	public void setConnectMark(Integer connectMark) {
		this.connectMark = connectMark;
	}
	public Integer getTurnMark() {
		return turnMark;
	}
	public void setTurnMark(Integer turnMark) {
		this.turnMark = turnMark;
	}
	public Integer getRevSpeed() {
		return revSpeed;
	}
	public void setRevSpeed(Integer revSpeed) {
		this.revSpeed = revSpeed;
	}
	public Integer getLoadSize() {
		return loadSize;
	}
	public void setLoadSize(Integer loadSize) {
		this.loadSize = loadSize;
	}
	public String getVehicleVin() {
		return vehicleVin;
	}
	public void setVehicleVin(String vehicleVin) {
		this.vehicleVin = vehicleVin;
	}
	public Integer getMileage() {
		return mileage;
	}
	public void setMileage(Integer mileage) {
		this.mileage = mileage;
	}
	public Integer getOilUse() {
		return oilUse;
	}
	public void setOilUse(Integer oilUse) {
		this.oilUse = oilUse;
	}
	public Integer getOilRest() {
		return oilRest;
	}
	public void setOilRest(Integer oilRest) {
		this.oilRest = oilRest;
	}
	public Integer getOilInst() {
		return oilInst;
	}
	public void setOilInst(Integer oilInst) {
		this.oilInst = oilInst;
	}
	public Integer getCanSpeed() {
		return canSpeed;
	}
	public void setCanSpeed(Integer canSpeed) {
		this.canSpeed = canSpeed;
	}
	public Integer getRotateSpeed() {
		return rotateSpeed;
	}
	public void setRotateSpeed(Integer rotateSpeed) {
		this.rotateSpeed = rotateSpeed;
	}
	public Integer getTorque() {
		return torque;
	}
	public void setTorque(Integer torque) {
		this.torque = torque;
	}
	public Integer getVoltage() {
		return voltage;
	}
	public void setVoltage(Integer voltage) {
		this.voltage = voltage;
	}
	public Integer getBrake() {
		return brake;
	}
	public void setBrake(Integer brake) {
		this.brake = brake;
	}
	public Integer getCoolantTemp() {
		return coolantTemp;
	}
	public void setCoolantTemp(Integer coolantTemp) {
		this.coolantTemp = coolantTemp;
	}
	public Integer getUrgentStatus() {
		return urgentStatus;
	}
	public void setUrgentStatus(Integer urgentStatus) {
		this.urgentStatus = urgentStatus;
	}
	public Date getUrgentStatusTime() {
		return urgentStatusTime;
	}
	public void setUrgentStatusTime(Date urgentStatusTime) {
		this.urgentStatusTime = urgentStatusTime;
	}
	
}
