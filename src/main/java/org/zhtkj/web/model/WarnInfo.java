package org.zhtkj.web.model;

import java.util.Date;

/**
 * ClassName: WarnInfo 
 * @Description: 报警信息表
 * @author Nikotesla
 * @date 2019年2月15日
 */
 
public class WarnInfo {

	private Integer warnInfoId;
	private String licensePlate;
	private Double latitude;
	private Double longitude;
	private Integer overload;
	private Integer heavySpeed;
	private Integer vioUnload;
	private Integer heavyBoxOpen;
	private Integer badDrive;
	private Integer heavyEnter;
	private Integer camera1Bad;
	private Integer camera2Bad;
	private Integer camera3Bad;
	private Integer camera4Bad;
	private Integer camera5Bad;
	private Integer ecuBad;
	private Integer driveMonitorBad;
	private Integer screenBad;
	private Integer loadBad;
	private Integer liftBad;
	private Integer boxBad;
	private Integer fagDriveBad;
	private Integer driverIdentityBad;
	private Integer driverFingerBad;
	private Date reportTime;
	private String yearMonth;
	
	//初始化时将报警标志全部置为0
	public WarnInfo() {
		this.overload = 0;
		this.heavySpeed = 0;
		this.vioUnload = 0;
		this.heavyBoxOpen = 0;
		this.badDrive = 0;
		this.heavyEnter = 0;
		this.camera1Bad = 0;
		this.camera2Bad = 0;
		this.camera3Bad = 0;
		this.camera4Bad = 0;
		this.camera5Bad = 0;
		this.ecuBad = 0;
		this.driveMonitorBad = 0;
		this.screenBad = 0;
		this.loadBad = 0;
		this.liftBad = 0;
		this.boxBad = 0;
		this.fagDriveBad = 0;
		this.driverIdentityBad = 0;
		this.driverFingerBad = 0;
	}

	/**
	 * @Description: 判断是否需要插入数据库，如果某一项报警存在才插入数据库
	 * @return   
	 * @return boolean  
	 * @throws
	 * @author Nikotesla
	 * @date 2019年1月22日
	 */
	public boolean isValid() {
		if (this.overload == 1 || this.heavySpeed == 1 || this.vioUnload == 1 || this.heavyBoxOpen == 1 || 
				this.badDrive == 1 || this.heavyEnter == 1 ||this.camera1Bad == 1 ||this.camera2Bad == 1 || 
				this.camera3Bad == 1 || this.camera4Bad == 1 || this.camera5Bad == 1 || this.ecuBad == 1 ||
				this.driveMonitorBad == 1 || this.screenBad == 1 || this.loadBad == 1 || this.liftBad == 1 || 
				this.boxBad == 1 || this.fagDriveBad == 1 || this.driverIdentityBad == 1 || this.driverFingerBad == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public Integer getWarnInfoId() {
		return warnInfoId;
	}

	public void setWarnInfoId(Integer warnInfoId) {
		this.warnInfoId = warnInfoId;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Integer getOverload() {
		return overload;
	}

	public void setOverload(Integer overload) {
		this.overload = overload;
	}

	public Integer getHeavySpeed() {
		return heavySpeed;
	}

	public void setHeavySpeed(Integer heavySpeed) {
		this.heavySpeed = heavySpeed;
	}

	public Integer getVioUnload() {
		return vioUnload;
	}

	public void setVioUnload(Integer vioUnload) {
		this.vioUnload = vioUnload;
	}

	public Integer getHeavyBoxOpen() {
		return heavyBoxOpen;
	}

	public void setHeavyBoxOpen(Integer heavyBoxOpen) {
		this.heavyBoxOpen = heavyBoxOpen;
	}

	public Integer getBadDrive() {
		return badDrive;
	}

	public void setBadDrive(Integer badDrive) {
		this.badDrive = badDrive;
	}

	public Integer getHeavyEnter() {
		return heavyEnter;
	}

	public void setHeavyEnter(Integer heavyEnter) {
		this.heavyEnter = heavyEnter;
	}

	public Integer getCamera1Bad() {
		return camera1Bad;
	}

	public void setCamera1Bad(Integer camera1Bad) {
		this.camera1Bad = camera1Bad;
	}

	public Integer getCamera2Bad() {
		return camera2Bad;
	}

	public void setCamera2Bad(Integer camera2Bad) {
		this.camera2Bad = camera2Bad;
	}

	public Integer getCamera3Bad() {
		return camera3Bad;
	}

	public void setCamera3Bad(Integer camera3Bad) {
		this.camera3Bad = camera3Bad;
	}

	public Integer getCamera4Bad() {
		return camera4Bad;
	}

	public void setCamera4Bad(Integer camera4Bad) {
		this.camera4Bad = camera4Bad;
	}

	public Integer getCamera5Bad() {
		return camera5Bad;
	}

	public void setCamera5Bad(Integer camera5Bad) {
		this.camera5Bad = camera5Bad;
	}

	public Integer getEcuBad() {
		return ecuBad;
	}

	public void setEcuBad(Integer ecuBad) {
		this.ecuBad = ecuBad;
	}

	public Integer getDriveMonitorBad() {
		return driveMonitorBad;
	}

	public void setDriveMonitorBad(Integer driveMonitorBad) {
		this.driveMonitorBad = driveMonitorBad;
	}

	public Integer getScreenBad() {
		return screenBad;
	}

	public void setScreenBad(Integer screenBad) {
		this.screenBad = screenBad;
	}

	public Integer getLoadBad() {
		return loadBad;
	}

	public void setLoadBad(Integer loadBad) {
		this.loadBad = loadBad;
	}

	public Integer getLiftBad() {
		return liftBad;
	}

	public void setLiftBad(Integer liftBad) {
		this.liftBad = liftBad;
	}

	public Integer getBoxBad() {
		return boxBad;
	}

	public void setBoxBad(Integer boxBad) {
		this.boxBad = boxBad;
	}

	public Integer getFagDriveBad() {
		return fagDriveBad;
	}

	public void setFagDriveBad(Integer fagDriveBad) {
		this.fagDriveBad = fagDriveBad;
	}

	public Integer getDriverIdentityBad() {
		return driverIdentityBad;
	}

	public void setDriverIdentityBad(Integer driverIdentityBad) {
		this.driverIdentityBad = driverIdentityBad;
	}

	public Integer getDriverFingerBad() {
		return driverFingerBad;
	}

	public void setDriverFingerBad(Integer driverFingerBad) {
		this.driverFingerBad = driverFingerBad;
	}

	public Date getReportTime() {
		return reportTime;
	}

	public void setReportTime(Date reportTime) {
		this.reportTime = reportTime;
	}

	public String getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}

}
