package org.zhtkj.web.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * ClassName: StatusInfo 
 * @Description: 车辆状态变化记录
 * @author Nikotesla
 * @date 2019年1月31日
 */
@TableName("zt_status_info")
public class StatusInfo {

	@TableId(type = IdType.AUTO)
	private Integer statusId;
	private String licensePlate;
	private String terminalId;
	private Integer empty;
	private Integer heavy;
	private Integer open;
	private Integer close;
	private Integer up;
	private Integer flat;
	private Double latitude;
	private Double longitude;
	private Integer altitude;
	private Integer speed;
	private Integer direction;
	private Date reportTime;
	
	//初始化时状态变化标志全部置为0
	public StatusInfo() {
		this.empty = 0;
		this.heavy = 0;
		this.open = 0;
		this.close = 0;
		this.up = 0;
		this.flat = 0;
	}

	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public Integer getEmpty() {
		return empty;
	}
	public void setEmpty(Integer empty) {
		this.empty = empty;
	}
	public Integer getHeavy() {
		return heavy;
	}
	public void setHeavy(Integer heavy) {
		this.heavy = heavy;
	}
	public Integer getOpen() {
		return open;
	}
	public void setOpen(Integer open) {
		this.open = open;
	}
	public Integer getClose() {
		return close;
	}
	public void setClose(Integer close) {
		this.close = close;
	}
	public Integer getUp() {
		return up;
	}
	public void setUp(Integer up) {
		this.up = up;
	}
	public Integer getFlat() {
		return flat;
	}
	public void setFlat(Integer flat) {
		this.flat = flat;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Integer getAltitude() {
		return altitude;
	}
	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}
	public Integer getSpeed() {
		return speed;
	}
	public void setSpeed(Integer speed) {
		this.speed = speed;
	}
	public Integer getDirection() {
		return direction;
	}
	public void setDirection(Integer direction) {
		this.direction = direction;
	}
	public Date getReportTime() {
		return reportTime;
	}
	public void setReportTime(Date reportTime) {
		this.reportTime = reportTime;
	}
	

	/**
	 * @Description: 判断是否需要插入数据库
	 * @return   
	 * @return boolean  
	 * @throws
	 * @author Nikotesla
	 * @date 2019年1月31日
	 */
	public boolean isInsert() {
		if (this.empty == 1 || this.heavy == 1 || this.open == 1 
				|| this.close == 1 ||  this.up == 1 || this.flat == 1) {
			return true;
		} else {
			return false;
		}
	}
}
