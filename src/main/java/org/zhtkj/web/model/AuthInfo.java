package org.zhtkj.web.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * ClassName: AuthInfo 
 * @Description: 鉴权信息
 * @author Nikotesla
 * @date 2019年1月16日
 */
@TableName(value = "zt_auth_info") 
public class AuthInfo {

	@TableId(type = IdType.INPUT)
	private String licensePlate;
	private String terminalId;
    
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
    
}
