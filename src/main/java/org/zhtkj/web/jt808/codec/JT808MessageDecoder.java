package org.zhtkj.web.jt808.codec;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zhtkj.framework.codec.MessageDecoder;
import org.zhtkj.framework.commons.transform.BitOperator;
import org.zhtkj.web.jt808.dto.basics.Header;

import io.netty.buffer.ByteBuf;

public class JT808MessageDecoder extends MessageDecoder {

    private static final Logger logger = LoggerFactory.getLogger(JT808MessageDecoder.class.getSimpleName());
    
    public JT808MessageDecoder(Charset charset) {
        super(charset);
    }

    @Override
    public Header decodeHeader(ByteBuf buf) {
        byte checkCode = buf.getByte(buf.readableBytes() - 1);
        buf = buf.slice(0, buf.readableBytes() - 1);
        byte calculatedCheckCode = BitOperator.xor(buf);
        if (checkCode != calculatedCheckCode) {
        	logger.info("校验码错误,checkCode=" + checkCode + ",calculatedCheckCode" + calculatedCheckCode);
        }
        return decode(buf, Header.class);
    }

}