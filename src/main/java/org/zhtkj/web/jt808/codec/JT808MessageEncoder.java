package org.zhtkj.web.jt808.codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.charset.Charset;

import org.zhtkj.framework.codec.MessageEncoder;
import org.zhtkj.framework.commons.transform.BitOperator;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

public class JT808MessageEncoder extends MessageEncoder<Header> {

    public JT808MessageEncoder(Charset charset) {
        super(charset);
    }

    @Override
    public ByteBuf encodeAll(PackageData<Header> body) {
        ByteBuf bodyBuf = encode(body);

        Header header = body.getHeader();
        header.setBodyLength(bodyBuf.readableBytes());

        ByteBuf headerBuf = encode(header);

        ByteBuf allBuf = Unpooled.wrappedBuffer(headerBuf, bodyBuf);
        byte checkCode = BitOperator.xor(allBuf);

        allBuf.writeByte(checkCode);

        return allBuf;
    }

}
