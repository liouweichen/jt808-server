package org.zhtkj.web.jt808.common;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class JT808MessageCode {

    //终端通用应答
    public static final int 终端通用应答 = 0x0001;
    //平台通用应答
    public static final int 平台通用应答 = 0x8001;
    //终端心跳
    public static final int 终端心跳 = 0x0002;
    //终端注册
    public static final int 终端注册 = 0x0100;
    //终端注册应答
    public static final int 终端注册应答 = 0x8100;
    //终端注销
    public static final int 终端注销 = 0x0003;
    //终端鉴权
    public static final int 终端鉴权 = 0x0102;
    //设置终端参数
    public static final int 设置终端参数 = 0x8103;
    //查询终端参数
    public static final int 查询终端参数 = 0x8104;
    //查询终端参数应答
    public static final int 查询终端参数应答 = 0x0104;
    //终端控制
    public static final int 终端控制 = 0x8105;
    //查询指定终端参数
    public static final int 查询指定终端参数 = 0x8106;
    //下发终端升级包
    public static final int 下发终端升级包 = 0x8108;
    //终端升级结果通知
    public static final int 终端升级结果通知 = 0x0108;
    //位置信息汇报
    public static final int 位置信息汇报 = 0x0200;
    //位置信息查询
    public static final int 位置信息查询 = 0x8201;
    //位置信息查询应答
    public static final int 位置信息查询应答 = 0x0201;
    //临时位置跟踪控制
    public static final int 临时位置跟踪控制 = 0x8202;
    //文本信息下发
    public static final int 文本信息下发 = 0x8300;
    //事件设置
    public static final int 事件设置 = 0x8301;
    //事件报告
    public static final int 事件报告 = 0x0301;
    //提问下发
    public static final int 提问下发 = 0x8302;
    //提问应答
    public static final int 提问应答 = 0x0302;
    //信息点播菜单设置
    public static final int 信息点播菜单设置 = 0x8303;
    //信息点播取消
    public static final int 信息点播取消 = 0x0303;
    //信息服务
    public static final int 信息服务 = 0x8304;
    //电话回拨
    public static final int 电话回拨 = 0x8400;
    //设置电话本
    public static final int 设置电话本 = 0x8401;
    //车辆控制
    public static final int 车辆控制 = 0x8500;
    //车辆控制应答
    public static final int 车辆控制应答 = 0x0500;
    //设置圆形区域
    public static final int 设置圆形区域 = 0x8600;
    //删除圆形区域
    public static final int 删除圆形区域 = 0x8601;
    //设置矩形区域
    public static final int 设置矩形区域 = 0x8602;
    //删除矩形区域
    public static final int 删除矩形区域 = 0x8603;
    //设置多边形区域
    public static final int 设置多边形区域 = 0x8604;
    //删除多边形区域
    public static final int 删除多边形区域 = 0x8605;
    //设置路线
    public static final int 设置路线 = 0x8606;
    //删除路线
    public static final int 删除路线 = 0x8607;
    //清空区域和路线(广州扩展协议)
    public static final int 清空区域和路线 = 0x8608;
    //行驶记录仪数据采集命令
    public static final int 行驶记录仪数据采集命令 = 0x8700;
    //行驶记录仪数据上传
    public static final int 行驶记录仪数据上传 = 0x0700;
    //行驶记录仪参数下传命令
    public static final int 行驶记录仪参数下传命令 = 0x8701;
    //电子运单上报
    public static final int 电子运单上报 = 0x0701;
    //驾驶员身份信息采集上报
    public static final int 驾驶员身份信息采集上报 = 0x0702;
    //定位数据批量上传
    public static final int 定位数据批量上传 = 0x0704;
    //CAN总线数据上传
    public static final int CAN总线数据上传 = 0x0705;
    //多媒体事件信息上传
    public static final int 多媒体事件信息上传 = 0x0800;
    //多媒体数据上传
    public static final int 多媒体数据上传 = 0x0801;
    //多媒体数据上传应答
    public static final int 多媒体数据上传应答 = 0x8800;
    //摄像头立即拍摄命令
    public static final int 摄像头立即拍摄命令 = 0x8801;
    //摄像头立即拍摄命令应答
    public static final int 摄像头立即拍摄命令应答 = 0x0805;
    //存储多媒体数据检索
    public static final int 存储多媒体数据检索 = 0x8802;
    //存储多媒体数据检索应答
    public static final int 存储多媒体数据检索应答 = 0x0802;
    //存储多媒体数据上传命令
    public static final int 存储多媒体数据上传命令 = 0x8803;
    //录音开始命令
    public static final int 录音开始命令 = 0x8804;
    //数据下行透传
    public static final int 数据下行透传 = 0x8900;
    //数据上行透传
    public static final int 数据上行透传 = 0x0900;
    //数据压缩上报
    public static final int 数据压缩上报 = 0x0901;
    //平台RSA公钥
    public static final int 平台RSA公钥 = 0x8A00;
    //终端RSA公钥
    public static final int 终端RSA公钥 = 0x0A00;
    //电子运单设置(广州扩展协议)
    public static final int 电子运单设置 = 0x8FF3;
    //电子运单删除(广州扩展协议)
    public static final int 电子运单删除 = 0x8FF4;
    //电子运单查询(广州扩展协议)
    public static final int 电子运单查询 = 0x8FF5;
    //电子运单查询应答(广州扩展协议)
    public static final int 电子运单查询应答 = 0x0FF5;
    //车外屏信息设置(广州扩展协议)
    public static final int 车外屏信息设置 = 0x8FF6;
    //车外屏信息删除(广州扩展协议)
    public static final int 车外屏信息删除 = 0x8FF7;
    //车外屏信息查询(广州扩展协议)
    public static final int 车外屏信息查询 = 0x8FF8;
    //车外屏信息查询应答(广州扩展协议)
    public static final int 车外屏信息查询应答 = 0x0FF8;
    //驾驶员身份信息设置(广州扩展协议)
    public static final int 驾驶员身份信息设置 = 0x8FF9;
    //驾驶员身份信息删除(广州扩展协议)
    public static final int 驾驶员身份信息删除 = 0x8FFA;
    //驾驶员身份信息查询(广州扩展协议)
    public static final int 驾驶员身份信息查询 = 0x8FFB;
    //驾驶员身份信息查询应答(广州扩展协议)
    public static final int 驾驶员身份信息查询应答 = 0x0FFB;
    //驾驶员身份验证信息上报(广州扩展协议)
    public static final int 驾驶员身份验证信息上报 = 0x0F00;
    //载重监测校准数据上报(广州扩展协议)
    public static final int 载重监测校准数据上报 = 0x0F01;
    //平台下行消息保留
    public static final int 平台下行消息保留 = 0x8F00 - 0x8FFF;
    //终端上行消息保留
    public static final int 终端上行消息保留 = 0x0F00 - 0x0FFF;
    
    public static final int 终端上传音视频资源列表 = 0x1205;
    public static final int 查询终端音视频属性 = 0x9003;
    public static final int 查询终端音视频属性响应 = 0x1003;
    public static final int 实时音视频传输请求 = 0x9101;
    public static final int 音视频实时传输控制 = 0x9102;
    public static final int 实时音视频传输状态通知 = 0x9105;
    public static final int 远程录像回放请求 = 0x9201;
    public static final int 远程录像回放控制 = 0x9202;
    
    //消息ID描述Map
    private static Map<Object, String> fieldMap;
    
    static {
    	fieldMap = new HashMap<Object, String>();
    	Field[] fields = JT808MessageCode.class.getDeclaredFields();
        for (Field field: fields) {
        	try {
				fieldMap.put(field.get(null), field.getName());
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
        }
    }
    
    /**
     * @Description: 根据消息ID获取消息描述
     * @param messageCode
     * @return 消息描述  
     * @return Object  
     * @throws
     * @author Nikotesla
     * @date 2019年1月11日
     */
    public static Object getMessageDesc(Integer messageCode) {
    	String desc = fieldMap.get(messageCode);
		return desc == null ? messageCode : desc;
    }
}