package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: MediaControl 
 * @Description: 音视频实时传输控制
 * @author Nikotesla
 * @date 2019年1月16日
 */
 
public class MediaControl extends PackageData<Header> {

	private Integer logicalChn;
	
	private Integer ctrlCmd;
	
	private Integer closeAVType;
	
	private Integer changeStreamType;

	@Property(index = 0, type = DataType.BYTE, desc = "逻辑通道号")
	public Integer getLogicalChn() {
		return logicalChn;
	}

	public void setLogicalChn(Integer logicalChn) {
		this.logicalChn = logicalChn;
	}

	@Property(index = 1, type = DataType.BYTE, desc = "控制指令")
	public Integer getCtrlCmd() {
		return ctrlCmd;
	}

	public void setCtrlCmd(Integer ctrlCmd) {
		this.ctrlCmd = ctrlCmd;
	}

	@Property(index = 2, type = DataType.BYTE, desc = "关闭音视频类型")
	public Integer getCloseAVType() {
		return closeAVType;
	}

	public void setCloseAVType(Integer closeAVType) {
		this.closeAVType = closeAVType;
	}

	@Property(index = 3, type = DataType.BYTE, desc = "切换码流类型")
	public Integer getChangeStreamType() {
		return changeStreamType;
	}

	public void setChangeStreamType(Integer changeStreamType) {
		this.changeStreamType = changeStreamType;
	}
	
}
