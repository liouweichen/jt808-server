package org.zhtkj.web.jt808.dto;

import java.util.ArrayList;
import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: ElectronWayBillQuery 
 * @Description: 电子运单查询
 * @author Nikotesla
 * @date 2019年1月19日
 */
 
public class ElectronWayBillQuery extends PackageData<Header> {

	private List<ElectronWayBill> electronWayBillList;

	@Property(index = 0, type = DataType.LIST, desc = "电子运单列表，如果此字段内容为空，则查询所有的电子运单")
	public List<ElectronWayBill> getElectronWayBillList() {
		return electronWayBillList;
	}

	public void setElectronWayBillList(List<ElectronWayBill> electronWayBillList) {
		this.electronWayBillList = electronWayBillList;
	}

	public void addElectronWayBill(ElectronWayBill electronWayBill) {
    	if (electronWayBillList == null) {
    		electronWayBillList = new ArrayList<ElectronWayBill>();
    	}
    	electronWayBillList.add(electronWayBill);
    }
}
