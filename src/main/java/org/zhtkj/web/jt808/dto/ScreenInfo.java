package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;

/**
 * ClassName: ScreenInfo 
 * @Description: 车外屏信息
 * @author Nikotesla
 * @date 2019年1月19日
 */
 
public class ScreenInfo {

	private Integer id;
	
	private Integer length;
	
	private String content;

	public ScreenInfo() {
		
	}
	
	public ScreenInfo(Integer id) {
		this.id = id;
	}
	
	@Property(index = 0, type = DataType.DWORD, desc = "车外屏信息ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Property(index = 4, type = DataType.BYTE, desc = "后续内容长度")
	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	@Property(index = 5, type = DataType.STRING, desc = "车外屏信息项内容(GBK编码)")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
