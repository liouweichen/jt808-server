package org.zhtkj.web.jt808.dto;

import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

public class ElectronWayBillQueryReply extends PackageData<Header> {

	private Integer total;
	
	private List<ElectronWayBill> electronWayBillList;

	@Property(index = 0, type = DataType.BYTE, desc = "电子运单总数")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Property(index = 1, type = DataType.LIST, desc = "电子运单列表")
	public List<ElectronWayBill> getElectronWayBillList() {
		return electronWayBillList;
	}

	public void setElectronWayBillList(List<ElectronWayBill> electronWayBillList) {
		this.electronWayBillList = electronWayBillList;
	}
	
}
