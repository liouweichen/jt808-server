package org.zhtkj.web.jt808.dto;

import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * 摄像头立即拍摄命令应答
 */
public class CameraShotReply extends PackageData<Header> {

    private Integer flowId;
    private Integer result;
    private Integer total;
    private List<MediaData> mediaDataList;

    public CameraShotReply() {
    }

    @Property(index = 0, type = DataType.WORD, desc = "应答流水号，对应平台摄像头立即拍摄命令的消息流水号")
    public Integer getFlowId() {
        return flowId;
    }

    public void setFlowId(Integer flowId) {
        this.flowId = flowId;
    }

    @Property(index = 2, type = DataType.BYTE, desc = "结果，0：成功；1：失败；2：通道不支持")
    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    @Property(index = 3, type = DataType.WORD, desc = "多媒体ID个数，n，拍摄成功的多媒体个数")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Property(index = 5, type = DataType.LIST, desc = "多媒体资源列表")
	public List<MediaData> getMediaDataList() {
		return mediaDataList;
	}

	public void setMediaDataList(List<MediaData> mediaDataList) {
		this.mediaDataList = mediaDataList;
	}
    
}