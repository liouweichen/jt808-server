package org.zhtkj.web.jt808.dto;

import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: DriverIdentityInfoSetting 
 * @Description: 驾驶员身份信息设置
 * @author Nikotesla
 * @date 2019年1月21日
 */
 
public class DriverIdentityInfoSetting extends PackageData<Header> {

	private Integer type;
	
	private Integer total;
	
	private List<DriverIdentityInfo> driverIdentityInfoList;

	@Property(index = 0, type = DataType.BYTE, desc = "设置属性，0：更新，1：追加，2：修改")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Property(index = 1, type = DataType.BYTE, desc = "驾驶员总数，取值范围：1～20")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Property(index = 2, type = DataType.LIST, desc = "驾驶员身份信息列表")
	public List<DriverIdentityInfo> getDriverIdentityInfoList() {
		return driverIdentityInfoList;
	}

	public void setDriverIdentityInfoList(List<DriverIdentityInfo> driverIdentityInfoList) {
		this.driverIdentityInfoList = driverIdentityInfoList;
	}
	
}
