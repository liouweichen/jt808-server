package org.zhtkj.web.jt808.dto;

import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * 电子运单上报
 */
public class ElectronWayBillReport extends PackageData<Header> {

    private Integer length;
    private List<Byte> content;
    
    @Property(index = 0, type = DataType.DWORD, desc = "电子运单长度")
	public Integer getLength() {
		return length;
	}
    
	public void setLength(Integer length) {
		this.length = length;
	}
	
	@Property(index = 4, type = DataType.LIST, desc = "电子运单内容")
	public List<Byte> getContent() {
		return content;
	}
	
	public void setContent(List<Byte> content) {
		this.content = content;
	}
    
}
