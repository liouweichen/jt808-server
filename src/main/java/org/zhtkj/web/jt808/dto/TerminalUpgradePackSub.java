package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * 下发终端升级包子包
 */
public class TerminalUpgradePackSub extends PackageData<Header> {

	private byte[] packet;

	@Property(index = 0, type = DataType.BYTES, desc = "数据包子包")
	public byte[] getPacket() {
		return packet;
	}

	public void setPacket(byte[] packet) {
		this.packet = packet;
	}
	
}
