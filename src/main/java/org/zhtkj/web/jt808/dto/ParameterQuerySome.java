package org.zhtkj.web.jt808.dto;

import java.util.ArrayList;
import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: ParameterQuerySome 
 * @Description: 查询指定终端参数
 * @author Nikotesla
 * @date 2019年1月25日
 */
 
public class ParameterQuerySome extends PackageData<Header> {

	private Integer total;
	
	private List<TerminalParameter> terminalParameterList;

	@Property(index = 0, type = DataType.BYTE, desc = "参数总数")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Property(index = 1, type = DataType.LIST, desc = "参数列表")
	public List<TerminalParameter> getTerminalParameterList() {
		return terminalParameterList;
	}

	public void setTerminalParameterList(List<TerminalParameter> terminalParameterList) {
		this.terminalParameterList = terminalParameterList;
	}
	
	public void addTerminalParameter(TerminalParameter terminalParameter) {
    	if (terminalParameterList == null) {
    		terminalParameterList = new ArrayList<TerminalParameter>();
    	}
    	terminalParameterList.add(terminalParameter);
    	total = terminalParameterList.size();
    }
}
