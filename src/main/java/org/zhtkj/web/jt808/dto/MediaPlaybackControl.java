package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: MediaPlaybackControl 
 * @Description: 平台下发远程录像回放控制
 * @author Nikotesla
 * @date 2019年1月16日
 */
 
public class MediaPlaybackControl extends PackageData<Header> {

	private Integer channelId;
	
	private Integer ctrlCmd;
	
	private Integer multiples;
	
	private String replayLocation;

	@Property(index = 0, type = DataType.BYTE, desc = "音视频通道号")
	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	@Property(index = 1, type = DataType.BYTE, desc = "回放控制，0：开始回放，1：暂停回放，2：结束回放，3：快进回放，4：关键帧快退回放，5：拖动回放，6：关键帧播放")
	public Integer getCtrlCmd() {
		return ctrlCmd;
	}

	public void setCtrlCmd(Integer ctrlCmd) {
		this.ctrlCmd = ctrlCmd;
	}

	@Property(index = 2, type = DataType.BYTE, desc = "快进或快退倍数")
	public Integer getMultiples() {
		return multiples;
	}

	public void setMultiples(Integer multiples) {
		this.multiples = multiples;
	}

	@Property(index = 3, type = DataType.BCD8421, desc = "拖动回放位置")
	public String getReplayLocation() {
		return replayLocation;
	}

	public void setReplayLocation(String replayLocation) {
		this.replayLocation = replayLocation;
	}
	
}
