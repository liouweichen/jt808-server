package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * 终端鉴权消息
 */
public class Authentication extends PackageData<Header> {

    private String authCode;

    @Property(index = 0, type = DataType.STRING, desc = "鉴权码,终端连接后上报鉴权码")
    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }
}