package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * 下发终端升级包
 */
public class TerminalUpgradePack extends PackageData<Header> {

    public static final int Terminal = 0;
    public static final int CardReader = 12;
    public static final int Beidou = 52;

    private Integer type;

    private byte[] manufacturerId;

    private Integer versionLen;

    private String version;

    private Integer packetLen;

    private byte[] packet;

    @Property(index = 0, type = DataType.BYTE, desc = "升级类型，0：终端，12：道路运输证 IC 卡读卡器，52：北斗卫星定位模块")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Property(index = 1, type = DataType.BYTES, length = 5, desc = "制造商ID,终端制造商编码")
    public byte[] getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(byte[] manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    @Property(index = 6, type = DataType.BYTE, desc = "版本号长度")
    public Integer getVersionLen() {
        return versionLen;
    }

    public void setVersionLen(Integer versionLen) {
        this.versionLen = versionLen;
    }

    @Property(index = 7, type = DataType.STRING, lengthName = "versionLen", desc = "版本号")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Property(index = 8, type = DataType.DWORD, indexOffsetName = "versionLen", desc = "数据包长度")
    public Integer getPacketLen() {
        return packetLen;
    }

    public void setPacketLen(Integer packetLen) {
        this.packetLen = packetLen;
    }

    @Property(index = 9, type = DataType.BYTES, indexOffsetName = "versionLen", lengthName = "packetLen", desc = "数据包")
    public byte[] getPacket() {
        return packet;
    }

    public void setPacket(byte[] packet) {
        this.packet = packet;
    }

}