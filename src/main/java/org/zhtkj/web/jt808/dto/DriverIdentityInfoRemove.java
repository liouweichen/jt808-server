package org.zhtkj.web.jt808.dto;

import java.util.ArrayList;
import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: DriverIdentityInfoRemove 
 * @Description: 驾驶员身份信息删除
 * @author Nikotesla
 * @date 2019年1月21日
 */
 
public class DriverIdentityInfoRemove extends PackageData<Header> {

	private Integer total;
	
	private List<DriverIdentityInfo> driverIdentityInfoList;

	@Property(index = 0, type = DataType.BYTE, desc = "驾驶员身份信息数，0：删除所有驾驶员身份信息")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Property(index = 1, type = DataType.LIST, desc = "驾驶员身份信息列表")
	public List<DriverIdentityInfo> getDriverIdentityInfoList() {
		return driverIdentityInfoList;
	}

	public void setDriverIdentityInfoList(List<DriverIdentityInfo> driverIdentityInfoList) {
		this.driverIdentityInfoList = driverIdentityInfoList;
	}
	
	public void addDriverIdentityInfo(DriverIdentityInfo driverIdentityInfo) {
    	if (driverIdentityInfoList == null) {
    		driverIdentityInfoList = new ArrayList<DriverIdentityInfo>();
    	}
    	driverIdentityInfoList.add(driverIdentityInfo);
    	total = driverIdentityInfoList.size();
    }
}
