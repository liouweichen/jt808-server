package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * 行驶记录数据上传
 */
public class DrivingRecordReport extends PackageData<Header> {

	private Integer replyFlowId;
	private byte commandWord;
	private byte[] data;
	
	@Property(index = 0, type = DataType.WORD, desc = "应答流水号")
	public Integer getReplyFlowId() {
		return replyFlowId;
	}
	
	public void setReplyFlowId(Integer replyFlowId) {
		this.replyFlowId = replyFlowId;
	}
	
	@Property(index = 2, type = DataType.BYTE, desc = "命令字")
	public byte getCommandWord() {
		return commandWord;
	}
	
	public void setCommandWord(byte commandWord) {
		this.commandWord = commandWord;
	}
	
	@Property(index = 3, type = DataType.BYTES, desc = "数据块")
	public byte[] getData() {
		return data;
	}
	
	public void setData(byte[] data) {
		this.data = data;
	}
	
	
}
