package org.zhtkj.web.jt808.dto;

import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: MediaPlaybackReply 
 * @Description: 平台下发远程录像回放请求应答
 * @author Nikotesla
 * @date 2019年1月16日
 */
 
public class MediaPlaybackReply extends PackageData<Header> {

	private Integer flowId;
	
	private Integer resourceTotal;
	
	private List<MediaResource> resourceList;
	
	@Property(index = 0, type = DataType.WORD, desc = "流水号，对应查询音视频资源列表指令的流水号")
	public Integer getFlowId() {
		return flowId;
	}

	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}

	@Property(index = 2, type = DataType.WORD, desc = "音视频资源总数")
	public Integer getResourceTotal() {
		return resourceTotal;
	}

	@Property(index = 6, type = DataType.LIST, desc = "音视频资源列表")
	public void setResourceTotal(Integer resourceTotal) {
		this.resourceTotal = resourceTotal;
	}

	public List<MediaResource> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<MediaResource> resourceList) {
		this.resourceList = resourceList;
	}

	public static class MediaResource {
		
		private Integer logicalChn;
		
		private String startTime;
		
		private String endTime;
		
		private byte[] warnMark;
		
		private Integer resourceType;
		
		private Integer streamType;
		
		private Integer storageType;
		
		private Integer fileSize;

		@Property(index = 0, type = DataType.BYTE, desc = "逻辑通道号")
		public Integer getLogicalChn() {
			return logicalChn;
		}

		public void setLogicalChn(Integer logicalChn) {
			this.logicalChn = logicalChn;
		}

		@Property(index = 1, type = DataType.STRING, desc = "开始时间")
		public String getStartTime() {
			return startTime;
		}

		public void setStartTime(String startTime) {
			this.startTime = startTime;
		}

		@Property(index = 7, type = DataType.STRING, desc = "结束时间")
		public String getEndTime() {
			return endTime;
		}

		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}

		@Property(index = 13, type = DataType.BYTES, length = 64, desc = "报警标志")
		public byte[] getWarnMark() {
			return warnMark;
		}

		public void setWarnMark(byte[] warnMark) {
			this.warnMark = warnMark;
		}

		@Property(index = 21, type = DataType.BYTE, desc = "音视频资源类型，0：音视频，1：音频，2：视频")
		public Integer getResourceType() {
			return resourceType;
		}

		public void setResourceType(Integer resourceType) {
			this.resourceType = resourceType;
		}

		@Property(index = 22, type = DataType.BYTE, desc = "码流类型，1：主码流，2：子码流")
		public Integer getStreamType() {
			return streamType;
		}

		public void setStreamType(Integer streamType) {
			this.streamType = streamType;
		}

		@Property(index = 23, type = DataType.BYTE, desc = "存储器类型，1：主存储器，2：灾备存储器")
		public Integer getStorageType() {
			return storageType;
		}

		public void setStorageType(Integer storageType) {
			this.storageType = storageType;
		}

		@Property(index = 24, type = DataType.DWORD, desc = "文件大小(单位字节)")
		public Integer getFileSize() {
			return fileSize;
		}

		public void setFileSize(Integer fileSize) {
			this.fileSize = fileSize;
		}
		
	}
	
}
