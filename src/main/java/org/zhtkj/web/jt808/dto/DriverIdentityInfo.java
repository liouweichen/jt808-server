package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;

/**
 * ClassName: DriverIdentityInfo 
 * @Description: 驾驶员身份信息
 * @author Nikotesla
 * @date 2019年1月21日
 */
 
public class DriverIdentityInfo {

	private Integer id;
	
	private Integer identityLength;
	
	private String idCard;
	
	private Integer fingerMarkLength;
	
	private byte[] fingerMark;

	public DriverIdentityInfo() {
		
	}
	
	public DriverIdentityInfo(Integer id) {
		this.id = id;
	}
	
	@Property(index = 0, type = DataType.DWORD, desc = "身份信息ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Property(index = 4, type = DataType.WORD, desc = "身份证ID长度")
	public Integer getIdentityLength() {
		return identityLength;
	}

	public void setIdentityLength(Integer identityLength) {
		this.identityLength = identityLength;
	}

	@Property(index = 6, type = DataType.STRING, desc = "身份证ID")
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Property(index = 24, type = DataType.WORD, desc = "指纹特征码长度")
	public Integer getFingerMarkLength() {
		return fingerMarkLength;
	}

	public void setFingerMarkLength(Integer fingerMarkLength) {
		this.fingerMarkLength = fingerMarkLength;
	}

	@Property(index = 26, type = DataType.BYTES, desc = "指纹特征码")
	public byte[] getFingerMark() {
		return fingerMark;
	}

	public void setFingerMark(byte[] fingerMark) {
		this.fingerMark = fingerMark;
	}
	
}
