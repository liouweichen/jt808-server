package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;

/**
 * ClassName: ElectronWayBill 
 * @Description: 电子运单
 * @author Nikotesla
 * @date 2019年1月19日
 */
 
public class ElectronWayBill {

	private Integer id;
	
	private String startTime;
	
	private String endTime;
	
	private Integer length;
	
	private String content;

	public ElectronWayBill() {
		
	}
	
	public ElectronWayBill(Integer id) {
		this.id = id;
	}
	
	@Property(index = 0, type = DataType.DWORD, desc = "电子运单ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Property(index = 4, type = DataType.BCD8421, desc = "开始时间，YYMMDD")
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Property(index = 8, type = DataType.BCD8421, desc = "结束时间，YYMMDD")
	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Property(index = 12, type = DataType.BYTE, desc = "后续内容长度")
	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	@Property(index = 13, type = DataType.STRING, desc = "内容，GBK编码，格式:工地名称,处置场名称,路线描述,核准证编号,工程名称")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
