package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: MediaTransStatusNotify 
 * @Description: 实时音视频传输状态通知
 * @author Nikotesla
 * @date 2019年1月16日
 */
 
public class MediaTransStatusNotify extends PackageData<Header> {

	private Integer logicalChn;
	
	private Integer lossRate;

	@Property(index = 0, type = DataType.BYTE, desc = "逻辑通道号")
	public Integer getLogicalChn() {
		return logicalChn;
	}

	public void setLogicalChn(Integer logicalChn) {
		this.logicalChn = logicalChn;
	}

	@Property(index = 1, type = DataType.BYTE, desc = "丢包率")
	public Integer getLossRate() {
		return lossRate;
	}

	public void setLossRate(Integer lossRate) {
		this.lossRate = lossRate;
	}
	
}
