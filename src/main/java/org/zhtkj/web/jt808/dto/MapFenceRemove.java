package org.zhtkj.web.jt808.dto;

import java.util.ArrayList;
import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * 删除区域 
 */ 
public class MapFenceRemove<T> extends PackageData<Header> {

	private Integer total;
	private List<MapFence> mapFences;
	
	@Property(index = 0, type = DataType.BYTE, desc = "区域ID个数")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Property(index = 1, type = DataType.LIST, desc = "区域列表")
	public List<MapFence> getMapFences() {
		return mapFences;
	}

	public void setMapFences(List<MapFence> mapFences) {
		this.mapFences = mapFences;
	}

    public void addMapFence(MapFence mapFence) {
    	if (mapFences == null) {
    		mapFences = new ArrayList<MapFence>();
    	}
    	mapFences.add(mapFence);
    	total = mapFences.size();
    }
    
}
