package org.zhtkj.web.jt808.dto;

import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: PositionBatchReport 
 * @Description: 定位数据批量上传
 * @author Nikotesla
 * @date 2019年1月18日
 */
 
public class PositionReportBatch extends PackageData<Header> {

	private Integer total;
	
	private Integer dataType;
	
	private List<PositionReportBatchData> positionReportBatchDataList;
	
	@Property(index = 0, type = DataType.WORD, desc = "数据项个数")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Property(index = 2, type = DataType.BYTE, desc = "位置数据类型，0：正常位置批量汇报，1：盲区补报")
	public Integer getDataType() {
		return dataType;
	}

	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}

	@Property(index = 3, type = DataType.LIST, desc = "位置汇报列表")
	public List<PositionReportBatchData> getPositionReportBatchDataList() {
		return positionReportBatchDataList;
	}

	public void setPositionReportBatchDataList(List<PositionReportBatchData> positionReportBatchDataList) {
		this.positionReportBatchDataList = positionReportBatchDataList;
	}

	public static class PositionReportBatchData {
		
		private Integer length;
		
		private PositionReport positionReport;
		
		@Property(index = 0, type = DataType.WORD, desc = "位置汇报数据体长度，n")
		public Integer getLength() {
			return length;
		}

		public void setLength(Integer length) {
			this.length = length;
		}

		@Property(index = 2, type = DataType.OBJ, lengthName = "length", desc = "位置汇报信息")
		public PositionReport getPositionReport() {
			return positionReport;
		}

		public void setPositionReport(PositionReport positionReport) {
			this.positionReport = positionReport;
		}
		
	}
}
