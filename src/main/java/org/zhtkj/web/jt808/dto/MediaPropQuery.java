package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: MediaPropQuery 
 * @Description: 查询终端音视频属性
 * @author Nikotesla
 * @date 2019年1月16日
 */
 
public class MediaPropQuery extends PackageData<Header> {

}
