package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: MediaTransRequest 
 * @Description: TODO
 * @author Nikotesla
 * @date 2019年1月16日
 */
 
public class MediaTransRequest extends PackageData<Header>{
	
	private Integer length;
	
	private String ipAddress;
	
	private Integer tcpPort;
	
	private Integer udpPort;
	
	private Integer channelId;
	
	private Integer dataType;
	
	private Integer codeType;
	
	 @Property(index=0, type=DataType.BYTE, desc="ip地址长度")
	 public Integer getLength() {
		 return length;
	 }
	 
	 public void setLength(Integer length) {
		 this.length = length;
	 }
	 
	 @Property(index=1, type=DataType.STRING, desc="服务器IP")
	 public String getIpAddress() {
		 return ipAddress;
	 }
	 
	 public void setIpAddress(String ipAddress) {
		 this.ipAddress = ipAddress;
	 }
	 
	 @Property(index=2, type=DataType.WORD, desc="TCP端口")
	 public Integer getTcpPort() {
		 return tcpPort;
	 }
	 
	 public void setTcpPort(Integer tcpPort) {
		 this.tcpPort = tcpPort;
	 }
	 
	 @Property(index=3, type=DataType.WORD, desc="UDP端口")
	 public Integer getUdpPort() {
		 return udpPort;
	 }
	 
	 public void setUdpPort(Integer udpPort) {
		 this.udpPort = udpPort;
	 }
	 
	 @Property(index=4, type=DataType.BYTE, desc="通道ID")
	 public Integer getChannelId() {
		 return channelId;
	 }
	 
	 public void setChannelId(Integer channelId) {
		 this.channelId = channelId;
	 }
	 
	 @Property(index=5, type=DataType.BYTE, desc="数据类型")
	 public Integer getDataType() {
		 return dataType;
	 }
	 
	 public void setDataType(Integer dataType) {
		 this.dataType = dataType;
	 }
	 
	 @Property(index=6, type=DataType.BYTE, desc="编码类型")
	 public Integer getCodeType() {
		 return codeType;
	 }
	 
	 public void setCodeType(Integer codeType) {
		 this.codeType = codeType;
	 }
	 
}
