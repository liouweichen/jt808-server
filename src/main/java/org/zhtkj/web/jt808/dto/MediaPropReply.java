package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: MediaPropReply 
 * @Description: 查询终端音视频属性响应
 * @author Nikotesla
 * @date 2019年1月16日
 */
 
public class MediaPropReply extends PackageData<Header> {
	
	private Integer audioCodeType;
	
	private Integer audioChannels;
	
	private Integer audioFreq;
	
	private Integer audioBits;
	
	private Integer audioFrameLength;
	
	private Integer audioOutput;
	
	private Integer videoCodeType;
	
	private Integer maxAudio;
	
	private Integer maxVideo;
	
	@Property(index = 0, type = DataType.BYTE, desc = "输入音频编码方式")
	public Integer getAudioCodeType() {
		return this.audioCodeType;
	}
	
	public void setAudioCodeType(Integer audioCodeType) {
		this.audioCodeType = audioCodeType;
	}
	
	@Property(index = 1, type = DataType.BYTE, desc = "输入音频声道数")
	public Integer getAudioChannels() {
		return this.audioChannels;
	}
	
	public void setAudioChannels(Integer audioChannels) {
		this.audioChannels = audioChannels;
	}
	
	@Property(index = 2, type = DataType.BYTE, desc = "输入音频采样率")
	public Integer getAudioFreq() {
		return this.audioFreq;
	}
	
	public void setAudioFreq(Integer audioFreq) {
		this.audioFreq = audioFreq;
	}
	
	@Property(index = 3, type = DataType.BYTE, desc = "输入音频采样位数")
	public Integer getAudioBits() {
		return this.audioBits;
	}
	
	public void setAudioBits(Integer audioBits) {
		this.audioBits = audioBits;
	}
	
	@Property(index = 4, type = DataType.WORD, desc = "输入音频帧长度")
	public Integer getAudioFrameLength() {
		return this.audioFrameLength;
	}
	
	public void setAudioFrameLength(Integer audioFrameLength) {
		this.audioFrameLength = audioFrameLength;
	}	
	
	@Property(index = 5, type = DataType.BYTE, desc = "是否支持音频输出")
	public Integer getAudioOutput() {
		return this.audioOutput;
	}
	
	public void setAudioOutput(Integer audioOutput) {
		this.audioOutput = audioOutput;
	}
	
	@Property(index = 6, type = DataType.BYTE, desc = "视频编码方式")
	public Integer getVideoCodeType() {
		return this.videoCodeType;
	}
	
	public void setVideoCodeType(Integer videoCodeType) {
		this.videoCodeType = videoCodeType;
	}
	
	@Property(index = 7, type = DataType.BYTE, desc = "终端支持的最大音频物理通道数量")
	public Integer getMaxAudio() {
		return this.maxAudio;
	}
	
	public void setMaxAudio(Integer maxAudio) {
		this.maxAudio = maxAudio;
	}	
	
	@Property(index = 8, type = DataType.BYTE, desc = "终端支持的最大视频物理通道数量")
	public Integer getMaxVideo() {
		return this.maxVideo;
	}
	
	public void setMaxVideo(Integer maxVideo) {
		this.maxVideo = maxVideo;
	}		
}
