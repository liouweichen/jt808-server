package org.zhtkj.web.jt808.dto;

import java.util.ArrayList;
import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: ScreenInfoQuery 
 * @Description: 车外屏信息查询
 * @author Nikotesla
 * @date 2019年1月19日
 */
 
public class ScreenInfoQuery extends PackageData<Header> {

	private List<ScreenInfo> screenInfoList;

	@Property(index = 0, type = DataType.LIST, desc = "车外屏信息列表")
	public List<ScreenInfo> getScreenInfoList() {
		return screenInfoList;
	}

	public void setScreenInfoList(List<ScreenInfo> screenInfoList) {
		this.screenInfoList = screenInfoList;
	}
	
	public void addScreenInfo(ScreenInfo screenInfo) {
    	if (screenInfoList == null) {
    		screenInfoList = new ArrayList<ScreenInfo>();
    	}
    	screenInfoList.add(screenInfo);
    }
}
