package org.zhtkj.web.jt808.dto;

import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: ScreenInfoQueryReply 
 * @Description: 车外屏信息查询应答
 * @author Nikotesla
 * @date 2019年1月19日
 */
 
public class ScreenInfoQueryReply extends PackageData<Header> {

	private Integer total;
	
	private List<ScreenInfo> screenInfoList;

	@Property(index = 0, type = DataType.BYTE, desc = "车外屏信息总数")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Property(index = 1, type = DataType.LIST, desc = "车外屏信息列表")
	public List<ScreenInfo> getScreenInfoList() {
		return screenInfoList;
	}

	public void setScreenInfoList(List<ScreenInfo> screenInfoList) {
		this.screenInfoList = screenInfoList;
	}
	
}
