package org.zhtkj.web.jt808.dto;

import java.util.ArrayList;
import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * 删除路线
 */
public class RouteRemove extends PackageData<Header> {

	private Integer routeTotal;
	private List<RouteSetting> routeSettings;
	
	@Property(index = 0, type = DataType.BYTE, desc = "路线数")
	public Integer getRouteTotal() {
		return routeTotal;
	}

	public void setRouteTotal(Integer routeTotal) {
		this.routeTotal = routeTotal;
	}

	@Property(index = 1, type = DataType.LIST, desc = "路线列表")
	public List<RouteSetting> getRouteSettings() {
		return routeSettings;
	}

	public void setRouteSettings(List<RouteSetting> routeSettings) {
		this.routeSettings = routeSettings;
	}

    public void addRouteSetting(RouteSetting routeSetting) {
    	if (routeSettings == null) {
    		routeSettings = new ArrayList<RouteSetting>();
    	}
    	routeSettings.add(routeSetting);
    	routeTotal = routeSettings.size();
    }
	
}
