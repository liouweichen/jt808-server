package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 车辆控制
 */
public class VehicleControl extends PackageData<Header> {

    private Integer sign;

    private int[] signs;

    @JsonIgnore
    @Property(index = 0, type = DataType.BYTE, desc = "控制标志")
    public Integer getSign() {
        return sign;
    }

    public void setSign(Integer sign) {
        this.sign = sign;
    }

    public int[] getSigns() {
        return signs;
    }

    public void setSigns(int[] signs) {
        int sign = 0;
        for (int b : signs) {
            sign |= 1 << b;
        }
        this.sign = sign;
        this.signs = signs;
    }

}