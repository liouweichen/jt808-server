package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: LoadAdjustDataReport 
 * @Description: 载重监测校准数据上报
 * @author Nikotesla
 * @date 2019年1月21日
 */
 
public class LoadAdjustDataReport extends PackageData<Header> {

	private String time;
	
	private Byte attribute;
	
	private Integer latitude;
	
	private Integer longitude;
	
	private Integer adjustRecord;
	
	private Byte adjustResult;
	
	private Integer floatingZero;

	private Integer fullLoadSign;

	@Property(index = 0, type = DataType.BCD8421, desc = "时间，YY-MM-DD-hh-mm-ss（GMT+8时间）")
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Property(index = 6, type = DataType.BYTE, desc = "属性，Bit0：纬度标示，0为北纬，1为南纬；Bit1：经度标示，0为东经，1为西经")
	public Byte getAttribute() {
		return attribute;
	}

	public void setAttribute(Byte attribute) {
		this.attribute = attribute;
	}

	@Property(index = 7, type = DataType.DWORD, desc = "纬度，以度为单位的纬度值乘以10的6次方，精确到百万分之一度")
	public Integer getLatitude() {
		return latitude;
	}

	public void setLatitude(Integer latitude) {
		this.latitude = latitude;
	}

	@Property(index = 11, type = DataType.DWORD, desc = "经度，以度为单位的经度值乘以10的6次方，精确到百万分之一度")
	public Integer getLongitude() {
		return longitude;
	}

	public void setLongitude(Integer longitude) {
		this.longitude = longitude;
	}

	@Property(index = 15, type = DataType.WORD, desc = "校准记录，修正成功或失败后加1，掉电保存")
	public Integer getAdjustRecord() {
		return adjustRecord;
	}

	public void setAdjustRecord(Integer adjustRecord) {
		this.adjustRecord = adjustRecord;
	}

	@Property(index = 17, type = DataType.BYTE, desc = "校准结果，1：校准成功，2：校准失败，保持原有参数")
	public Byte getAdjustResult() {
		return adjustResult;
	}

	public void setAdjustResult(Byte adjustResult) {
		this.adjustResult = adjustResult;
	}

	@Property(index = 18, type = DataType.WORD, desc = "浮动零点，校准后的浮动零点，精度：0.001")
	public Integer getFloatingZero() {
		return floatingZero;
	}

	public void setFloatingZero(Integer floatingZero) {
		this.floatingZero = floatingZero;
	}

	@Property(index = 20, type = DataType.WORD, desc = "满载标定，校准后的满载标定，精度：0.001")
	public Integer getFullLoadSign() {
		return fullLoadSign;
	}

	public void setFullLoadSign(Integer fullLoadSign) {
		this.fullLoadSign = fullLoadSign;
	}
	
}
