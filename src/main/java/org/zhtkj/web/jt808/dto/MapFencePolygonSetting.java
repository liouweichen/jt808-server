package org.zhtkj.web.jt808.dto;

import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: MapFencePolygonSetting 
 * @Description: 设置多边形区域
 * @author Nikotesla
 * @date 2019年1月14日
 */
 
public class MapFencePolygonSetting extends PackageData<Header> {

    private Long id;

    private Integer attribute;

    private String startTime;

    private String endTime;

    private Integer maxSpeed;

    private Integer duration;

    private Integer vertexTotal;

    private List<Vertex> vertexList;

    @Property(index = 0, type = DataType.DWORD, desc = "区域ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Property(index = 4, type = DataType.WORD, desc = "区域属性")
    public Integer getAttribute() {
        return attribute;
    }

    public void setAttribute(Integer attribute) {
        this.attribute = attribute;
    }

    @Property(index = 6, type = DataType.BCD8421, length = 6, desc = "起始时间")
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @Property(index = 12, type = DataType.BCD8421, length = 6, desc = "结束时间")
    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Property(index = 18, type = DataType.WORD, desc = "最高速度")
    public Integer getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Property(index = 20, type = DataType.BYTE, desc = "超速持续时间")
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Property(index = 21, type = DataType.WORD, desc = "区域总顶点数")
    public Integer getVertexTotal() {
        return vertexTotal;
    }

    public void setVertexTotal(Integer vertexTotal) {
        this.vertexTotal = vertexTotal;
    }

    @Property(index = 23, type = DataType.LIST, desc = "顶点列表")
    public List<Vertex> getVertexList() {
        return vertexList;
    }

    public void setVertexList(List<Vertex> vertexList) {
        this.vertexList = vertexList;
    }
    
    public static class Vertex {
    	
        private Long latitude;
        private Long longitude;

        public Vertex() {
        }

        public Vertex(Long latitude, Long longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Property(index = 0, type = DataType.DWORD, desc = "纬度")
        public Long getLatitude() {
            return latitude;
        }

        public void setLatitude(Long latitude) {
            this.latitude = latitude;
        }

        @Property(index = 4, type = DataType.DWORD, desc = "经度")
        public Long getLongitude() {
            return longitude;
        }

        public void setLongitude(Long longitude) {
            this.longitude = longitude;
        }
    }
}