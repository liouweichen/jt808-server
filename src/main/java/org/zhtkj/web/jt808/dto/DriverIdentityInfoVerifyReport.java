package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: DriverIdentityInfoVerifyReport 
 * @Description: 驾驶员身份验证信息上报
 * @author Nikotesla
 * @date 2019年1月21日
 */
 
public class DriverIdentityInfoVerifyReport extends PackageData<Header> {

	private String time;
	
	private Byte attribute;
	
	private Integer latitude;
	
	private Integer longitude;
	
	private Byte verifyType;
	
	private String idCard;

	@Property(index = 0, type = DataType.BCD8421, desc = "时间，YYMMDDHHmmss（GMT+8时间）")
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Property(index = 6, type = DataType.BYTE, desc = "Bit0：纬度标示，0为北纬，1为南纬；Bit1：经度标示，0为东经，1为西经")
	public Byte getAttribute() {
		return attribute;
	}

	public void setAttribute(Byte attribute) {
		this.attribute = attribute;
	}

	@Property(index = 7, type = DataType.DWORD, desc = "以度为单位的纬度值乘以10的6次方，精确到百万分之一度")
	public Integer getLatitude() {
		return latitude;
	}

	public void setLatitude(Integer latitude) {
		this.latitude = latitude;
	}

	@Property(index = 11, type = DataType.DWORD, desc = "以度为单位的经度值乘以10的6次方，精确到百万分之一度")
	public Integer getLongitude() {
		return longitude;
	}

	public void setLongitude(Integer longitude) {
		this.longitude = longitude;
	}

	@Property(index = 15, type = DataType.BYTE, desc = "身份验证方式，0：身份证，1：指纹")
	public Byte getVerifyType() {
		return verifyType;
	}

	public void setVerifyType(Byte verifyType) {
		this.verifyType = verifyType;
	}

	@Property(index = 16, type = DataType.STRING, desc = "身份证ID，GBK编码")
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	
}
