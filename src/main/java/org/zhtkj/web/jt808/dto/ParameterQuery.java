package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: ParameterQuery 
 * @Description: 查询终端参数
 * @author Nikotesla
 * @date 2019年1月25日
 */
 
public class ParameterQuery extends PackageData<Header> {

}
