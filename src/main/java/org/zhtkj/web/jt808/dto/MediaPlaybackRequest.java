package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: MediaPlaybackRequest 
 * @Description: 平台下发远程录像回放请求
 * @author Nikotesla
 * @date 2019年1月16日
 */
 
public class MediaPlaybackRequest extends PackageData<Header> {

	private Integer length;
	
	private String ipAddress;
	
	private Integer tcpPort;
	
	private Integer udpPort;
	
	private Integer logicalChn;
	
	private Integer mediaType;
	
	private Integer streamType;
	
	private Integer storageType;
	
	private Integer replayType;
	
	private Integer multiples;
	
	private String startTime;
	
	private String endTime;

	@Property(index = 0, type = DataType.BYTE, desc = "服务器ip地址长度")
	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	@Property(index = 1, type = DataType.STRING, desc = "服务器ip地址")
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Property(index = 2, type = DataType.WORD, desc = "服务器音视频通道监听端口号(TCP)")
	public Integer getTcpPort() {
		return tcpPort;
	}

	public void setTcpPort(Integer tcpPort) {
		this.tcpPort = tcpPort;
	}

	@Property(index = 4, type = DataType.WORD, desc = "服务器音视频通道监听端口号(UDP)")
	public Integer getUdpPort() {
		return udpPort;
	}

	public void setUdpPort(Integer udpPort) {
		this.udpPort = udpPort;
	}

	@Property(index = 5, type = DataType.BYTE, desc = "逻辑通道号")
	public Integer getLogicalChn() {
		return logicalChn;
	}

	public void setLogicalChn(Integer logicalChn) {
		this.logicalChn = logicalChn;
	}

	@Property(index = 6, type = DataType.BYTE, desc = "音视频类型")
	public Integer getMediaType() {
		return mediaType;
	}

	public void setMediaType(Integer mediaType) {
		this.mediaType = mediaType;
	}

	@Property(index = 7, type = DataType.BYTE, desc = "码流类型")
	public Integer getStreamType() {
		return streamType;
	}

	public void setStreamType(Integer streamType) {
		this.streamType = streamType;
	}

	@Property(index = 8, type = DataType.BYTE, desc = "存储器类型")
	public Integer getStorageType() {
		return storageType;
	}

	public void setStorageType(Integer storageType) {
		this.storageType = storageType;
	}

	@Property(index = 9, type = DataType.BYTE, desc = "回放方式")
	public Integer getReplayType() {
		return replayType;
	}

	public void setReplayType(Integer replayType) {
		this.replayType = replayType;
	}

	@Property(index = 10, type = DataType.BYTE, desc = "快进或快退倍数")
	public Integer getMultiples() {
		return multiples;
	}

	public void setMultiples(Integer multiples) {
		this.multiples = multiples;
	}

	@Property(index = 11, type = DataType.BCD8421, desc = "开始时间")
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Property(index = 17, type = DataType.BCD8421, desc = "结束时间")
	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
}
