package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;

/**
 * ClassName: MediaData 
 * @Description: 多媒体资源
 * @author Nikotesla
 * @date 2019年1月23日
 */
 
public class MediaData {
	
	private Integer id;

	@Property(index = 0, type = DataType.DWORD, desc = "多媒体ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
