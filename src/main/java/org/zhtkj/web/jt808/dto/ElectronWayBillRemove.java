package org.zhtkj.web.jt808.dto;

import java.util.ArrayList;
import java.util.List;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: ElectronWayBillRemove 
 * @Description: 电子运单删除
 * @author Nikotesla
 * @date 2019年1月19日
 */
 
public class ElectronWayBillRemove extends PackageData<Header> {

	private Integer total;
	
	private List<ElectronWayBill> electronWayBillList;

	@Property(index = 0, type = DataType.BYTE, desc = "电子运单数，0：删除所有电子运单")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Property(index = 1, type = DataType.LIST, desc = "电子运单列表")
    public List<ElectronWayBill> getElectronWayBillList() {
		return electronWayBillList;
	}

	public void setElectronWayBillList(List<ElectronWayBill> electronWayBillList) {
		this.electronWayBillList = electronWayBillList;
	}

	public void addElectronWayBill(ElectronWayBill electronWayBill) {
    	if (electronWayBillList == null) {
    		electronWayBillList = new ArrayList<ElectronWayBill>();
    	}
    	electronWayBillList.add(electronWayBill);
    	total = electronWayBillList.size();
    }
}
