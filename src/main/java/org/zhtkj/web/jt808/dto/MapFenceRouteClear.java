package org.zhtkj.web.jt808.dto;

import org.zhtkj.framework.annotation.Property;
import org.zhtkj.framework.enums.DataType;
import org.zhtkj.framework.message.PackageData;
import org.zhtkj.web.jt808.dto.basics.Header;

/**
 * ClassName: ClearFenceRoute 
 * @Description: 清空区域和路线
 * @author Nikotesla
 * @date 2019年1月11日
 */
 
public class MapFenceRouteClear extends PackageData<Header> {

	private Integer command;

	@Property(index = 0, type = DataType.BYTE, desc = "命令值，0：清空所有区域和路线  1：清空所有区域  3：清空所有路线")
	public Integer getCommand() {
		return command;
	}

	public void setCommand(Integer command) {
		this.command = command;
	}
	
}
