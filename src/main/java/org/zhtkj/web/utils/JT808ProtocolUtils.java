package org.zhtkj.web.utils;

import java.util.ArrayList;
import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

/**
 * ClassName: JT808ProtocolUtils 
 * @Description: JT808协议转义工具类
 * @author Nikotesla
 * @date 2019年1月15日
 */
 
public class JT808ProtocolUtils {
	
    /**
     * @Description: 接收消息时反转义
     * @param buf
     * @return
     * @return ByteBuf  
     * @throws
     * @author Nikotesla
     * @date 2019年1月15日
     */
    public static ByteBuf doEscape4Receive(ByteBuf buf) {
    	ByteBuf newBuf = Unpooled.buffer();
        List<Byte> newBytes = new ArrayList<Byte>();
        int size = buf.readableBytes();
        for (int i = 0; i < size; i++) {
        	if (buf.getByte(i) == 0x7d && buf.getByte(i + 1) == (byte) 0x02) {
        		newBytes.add((byte) 0x7E);
        		i++;
        	} else if (buf.getByte(i) == 0x7d && buf.getByte(i + 1) == (byte) 0x01) {
        		newBytes.add((byte) 0x7D);
        		i++;
        	} else {
        		newBytes.add(buf.getByte(i));
        	}
        }
        for (int i = 0; i < newBytes.size(); i++) {
        	newBuf.writeByte(newBytes.get(i));
        }
        return newBuf;
    }

    /**
     * @Description: 发送消息时转义
     * @param buf
     * @return
     * @return ByteBuf  
     * @throws
     * @author Nikotesla
     * @date 2019年1月15日
     */
    public static ByteBuf doEscape4Send(ByteBuf buf) {
    	ByteBuf newBuf = Unpooled.buffer();
        List<Byte> newBytes = new ArrayList<Byte>();
        int size = buf.readableBytes();
        for (int i = 0; i < size; i++) {
        	if (buf.getByte(i) == 0x7E) {
        		newBytes.add((byte) 0x7D);
        		newBytes.add((byte) 0x02);
        	} else if (buf.getByte(i) == 0x7D) {
        		newBytes.add((byte) 0x7D);
        		newBytes.add((byte) 0x01);
        	} else {
        		newBytes.add(buf.getByte(i));
        	}
        }
        for (int i = 0; i < newBytes.size(); i++) {
        	newBuf.writeByte(newBytes.get(i));
        }
        return newBuf;
    }

}