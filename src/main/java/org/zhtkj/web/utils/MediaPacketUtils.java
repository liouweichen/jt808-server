package org.zhtkj.web.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.joda.time.DateTime;

public class MediaPacketUtils {

	//key:terminalId+流水号，value：平台下发指令信息
	public static Map<String, ActionInfo> actionMap = new ConcurrentHashMap<String, ActionInfo>();
	//key:terminalId，value：多媒体数据包信息
	public static Map<String, PacketInfo> packetMap = new ConcurrentHashMap<String, PacketInfo>();
	
	public static ActionInfo getAction(String key) {
		return actionMap.get(key);
	}
	
	public static ActionInfo putAction(String key, ActionInfo actionInfo) {
		return actionMap.put(key, actionInfo);
	}
	
	public static PacketInfo getPacket(String key) {
		return packetMap.get(key);
	}
	
	public static PacketInfo putPacket(String key, PacketInfo packetInfo) {
		return packetMap.put(key, packetInfo);
	}
	
	public static void removeActionInfo(String key) {
		actionMap.remove(key);
	}
	
	public static void removePacketInfo(String key) {
		packetMap.remove(key);
	}
	
	public static class ActionInfo {
		
		private DateTime receiveTime; //初始化时间
		private Integer actionId; //平台下发指令ID
		
		public ActionInfo(DateTime receiveTime, Integer actionId) {
			this.receiveTime = receiveTime;
			this.actionId = actionId;
		}
		
		public DateTime getReceiveTime() {
			return receiveTime;
		}
		public void setReceiveTime(DateTime receiveTime) {
			this.receiveTime = receiveTime;
		}
		public Integer getActionId() {
			return actionId;
		}
		public void setActionId(Integer actionId) {
			this.actionId = actionId;
		}
		
	}
	
	public static class PacketInfo {
		
		public PacketInfo(DateTime receiveTime, Object[] packageDatas) {
			this.receiveTime = receiveTime;
			this.packageDatas = packageDatas;
		}
		
		private DateTime receiveTime; //初始化时间
		private Object[] packageDatas; //分包数据

		public DateTime getReceiveTime() {
			return receiveTime;
		}

		public void setReceiveTime(DateTime receiveTime) {
			this.receiveTime = receiveTime;
		}

		public Object[] getPackageDatas() {
			return packageDatas;
		}

		public void setPackageDatas(Object[] packageDatas) {
			this.packageDatas = packageDatas;
		}
		
	}
	
}
