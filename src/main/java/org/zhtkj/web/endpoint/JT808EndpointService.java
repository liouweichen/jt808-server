package org.zhtkj.web.endpoint;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.zhtkj.web.dao.WarnInfoMapper;
import org.zhtkj.web.model.HistoryInfo;
import org.zhtkj.web.model.HistoryWarn;
import org.zhtkj.web.model.WarnInfo;

@Service
public class JT808EndpointService {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    
    @Autowired
    private WarnInfoMapper warnInfoMapper;
    
    /**
     * @Description: 处理报警信息 ,每种报警隔一定时间才会插入数据库
     * @return void  
     * @author Nikotesla
     * @date 2019年2月15日
     */
    public void handlerWarnInfo(HistoryInfo hisInfo,HistoryWarn hisWarn, String[] connectBit) {
		WarnInfo warnInfo = new WarnInfo();
		String key = "warn:" + hisWarn.getTerminalId();
		HashOperations<String, Object, Object> ops = redisTemplate.opsForHash();
		Long now = System.currentTimeMillis();
		//超载
		if (hisWarn.getOverload() == 1) {
			Object overload = ops.get(key, "overload");
			if (overload == null || ((Long) overload + 3600000) < now) {
				warnInfo.setOverload(1);
				ops.put(key, "overload", now);
				redisTemplate.expire(key, 1, TimeUnit.HOURS);
			}
		}
		//重车超速
		if (hisWarn.getHeavySpeed() == 1) {
			Object heavySpeed = ops.get(key, "heavySpeed");
			if (heavySpeed == null || ((Long) heavySpeed + 3600000) < now) {
				warnInfo.setHeavySpeed(1);
				ops.put(key, "heavySpeed", now);
				redisTemplate.expire(key, 1, TimeUnit.HOURS);
			}
		}
		//违规区域卸载
		if (hisWarn.getVioUnload() == 1) {
			Object vioUnload = ops.get(key, "vioUnload");
			if (vioUnload == null || ((Long) vioUnload + 3600000) < now) {
				warnInfo.setVioUnload(1);
				ops.put(key, "vioUnload", now);
				redisTemplate.expire(key, 1, TimeUnit.HOURS);
			}
		}
		//重车行驶厢盖未关闭
		if (hisWarn.getHeavyBoxOpen() == 1) {
			Object heavyBoxOpen = ops.get(key, "heavyBoxOpen");
			if (heavyBoxOpen == null || ((Long) heavyBoxOpen + 3600000) < now) {
				warnInfo.setHeavyBoxOpen(1);
				ops.put(key, "heavyBoxOpen", now);
				redisTemplate.expire(key, 1, TimeUnit.HOURS);
			}
		}
		//疲劳状态驾驶或不规范驾驶
		if (hisWarn.getHeavyBoxOpen() == 1) {
			Object badDrive = ops.get(key, "badDrive");
			if (badDrive == null || ((Long) badDrive + 3600000) < now) {
				warnInfo.setBadDrive(1);
				ops.put(key, "badDrive", now);
				redisTemplate.expire(key, 1, TimeUnit.HOURS);
			}
		}
		//疲劳状态驾驶或不规范驾驶
		if (hisWarn.getHeavyEnter() == 1) {
			Object heavyEnter = ops.get(key, "heavyEnter");
			if (heavyEnter == null || ((Long) heavyEnter + 3600000) < now) {
				warnInfo.setHeavyEnter(1);
				ops.put(key, "heavyEnter", now);
				redisTemplate.expire(key, 1, TimeUnit.HOURS);
			}
		}
		//连接报警解析
		if (connectBit != null) {
			//1路摄像头（货箱图像）未连接
			if (Integer.parseInt(connectBit[4]) == 0) {
				Object camera1Bad = ops.get(key, "camera1Bad");
				if (camera1Bad == null || ((Long) camera1Bad + 3600000) < now) {
					warnInfo.setCamera1Bad(1);
					ops.put(key, "camera1Bad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//2路摄像头（驾驶员图像）未连接
			if (Integer.parseInt(connectBit[5]) == 0) {
				Object camera2Bad = ops.get(key, "camera2Bad");
				if (camera2Bad == null || ((Long) camera2Bad + 3600000) < now) {
					warnInfo.setCamera2Bad(1);
					ops.put(key, "camera2Bad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//3路摄像头（前方路面图像）未连接
			if (Integer.parseInt(connectBit[6]) == 0) {
				Object camera3Bad = ops.get(key, "camera3Bad");
				if (camera3Bad == null || ((Long) camera3Bad + 3600000) < now) {
					warnInfo.setCamera3Bad(1);
					ops.put(key, "camera3Bad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//4路摄像头（右侧盲区图像）未连接
			if (Integer.parseInt(connectBit[7]) == 0) {
				Object camera4Bad = ops.get(key, "camera4Bad");
				if (camera4Bad == null || ((Long) camera4Bad + 3600000) < now) {
					warnInfo.setCamera4Bad(1);
					ops.put(key, "camera4Bad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//5路摄像头（倒车后视图像）未连接
			if (Integer.parseInt(connectBit[8]) == 0) {
				Object camera5Bad = ops.get(key, "camera5Bad");
				if (camera5Bad == null || ((Long) camera5Bad + 3600000) < now) {
					warnInfo.setCamera5Bad(1);
					ops.put(key, "camera5Bad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//ECU通信未连接
			if (Integer.parseInt(connectBit[9]) == 0) {
				Object ecuBad = ops.get(key, "ecuBad");
				if (ecuBad == null || ((Long) ecuBad + 3600000) < now) {
					warnInfo.setEcuBad(1);
					ops.put(key, "ecuBad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//不良驾驶行为监测未连接
			if (Integer.parseInt(connectBit[10]) == 0) {
				Object driveMonitorBad = ops.get(key, "driveMonitorBad");
				if (driveMonitorBad == null || ((Long) driveMonitorBad + 3600000) < now) {
					warnInfo.setDriveMonitorBad(1);
					ops.put(key, "driveMonitorBad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//车载显示屏未连接
			if (Integer.parseInt(connectBit[14]) == 0) {
				Object screenBad = ops.get(key, "screenBad");
				if (screenBad == null || ((Long) screenBad + 3600000) < now) {
					warnInfo.setScreenBad(1);
					ops.put(key, "screenBad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//车辆载重监测未连接
			if (Integer.parseInt(connectBit[19]) == 0) {
				Object loadBad = ops.get(key, "loadBad");
				if (loadBad == null || ((Long) loadBad + 3600000) < now) {
					warnInfo.setLoadBad(1);
					ops.put(key, "loadBad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//车厢举升监测未连接
			if (Integer.parseInt(connectBit[20]) == 0) {
				Object liftBad = ops.get(key, "liftBad");
				if (liftBad == null || ((Long) liftBad + 3600000) < now) {
					warnInfo.setLiftBad(1);
					ops.put(key, "liftBad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//厢门闭合监测未连接
			if (Integer.parseInt(connectBit[21]) == 0) {
				Object boxBad = ops.get(key, "boxBad");
				if (boxBad == null || ((Long) boxBad + 3600000) < now) {
					warnInfo.setBoxBad(1);
					ops.put(key, "boxBad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//疲劳驾驶或不规范驾驶监测未连接
			if (Integer.parseInt(connectBit[22]) == 0) {
				Object fagDriveBad = ops.get(key, "fagDriveBad");
				if (fagDriveBad == null || ((Long) fagDriveBad + 3600000) < now) {
					warnInfo.setFagDriveBad(1);
					ops.put(key, "fagDriveBad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//驾驶员身份识别（身份证ID采集）未连接
			if (Integer.parseInt(connectBit[23]) == 0) {
				Object driverIdentityBad = ops.get(key, "driverIdentityBad");
				if (driverIdentityBad == null || ((Long) driverIdentityBad + 3600000) < now) {
					warnInfo.setDriverIdentityBad(1);
					ops.put(key, "driverIdentityBad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
			//驾驶员身份识别（指纹采集）未连接
			if (Integer.parseInt(connectBit[24]) == 0) {
				Object driverFingerBad = ops.get(key, "driverFingerBad");
				if (driverFingerBad == null || ((Long) driverFingerBad + 3600000) < now) {
					warnInfo.setDriverFingerBad(1);
					ops.put(key, "driverFingerBad", now);
					redisTemplate.expire(key, 1, TimeUnit.HOURS);
				}
			}
		}
		if (warnInfo.isValid()) {
			warnInfo.setLicensePlate(hisWarn.getLicensePlate());
			warnInfo.setLatitude(hisInfo.getLatitude());
			warnInfo.setLongitude(hisInfo.getLongitude());
			warnInfo.setReportTime(hisWarn.getReportTime());
			warnInfo.setYearMonth(hisWarn.getYearMonth());
			warnInfoMapper.insertWarnInfo(warnInfo);
		}
    }
}
